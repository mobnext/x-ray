/* eslint-disable no-undef */
/* eslint-disable no-lonely-if */
/* eslint-disable prefer-const */
/* eslint-disable indent */
import React, { useContext, useState, useEffect, createContext } from 'react';
import socketIO from "socket.io-client";
import { connect } from 'react-redux';
// import carIcon from '';
import IconCar from '../images/icons/icon_car.png';
import IconPin from '../images/icons/icon_pin.png';
import MyCar from '../images/icons/ico_car.svg';
import DropIcon from '../images/icons/ico_map_dropoff.png';

export const Socket = createContext();
// const socket;
// let dir = {};

let marker;
const SocketContext = (props) => {
    const [location, setlocation] = useState({});
    const [dir, setDir] = useState(null);
    const [pickUpSource, setpickUpSource] = useState({});
    const [dropLocation, setDropLocation] = useState({});
    const [arrived, setArrived] = useState(false);
    const [startRide, setStartRide] = useState(false);
    const [driverLocation, setDriverLocation] = useState({});
    const [rideAccepted, setRideAccepted] = useState(false);
    const [socket, setSocket] = useState(null);
    const [mapRef, setMapRef] = useState('');
    const [mapCarMarker, setMapCarMarker] = useState(null);
    const [mapSourceMarker, setMapSourceMarker] = useState(null);
    const [mapDestinationMarker, setMapDestinationMarker] = useState(null);
    const [text, setText] = useState('Your Request has accepted');


    const forDirection = (id) => {

        // console.log(`----------***********************************************${id}`);
        socket.emit('startRideTrackingEvent', { jobId: id });


        // console.log("listen ride useeffect with pickup", pickUpSource);
        // if (Object.keys(pickUpSource).length > 0) {
        // console.log("listen ride useeffect with pickup", pickUpSource);
        // console.log(socket);

        socket.on('listenRideTrackingEvent', (data) => {

            // console.log('================Driver Location====================');
            // console.log(data);
            // // console.log(pickUpSource);

            localStorage.setItem('driverlocation', JSON.stringify(data.socket.resource));

            if (data.socket.resource.jobStatus == 2 && data.socket.resource.isScheduled) {
                return;
            }


            setDriverLocation(data);




            // console.log('====================================');


        });
        // }


    }


    function makeMarkerCar(newPosition, icon, bearing, lat, lng) {

        let previousLocation;

        if (JSON.parse(localStorage.getItem('previousPosition'))) {

            previousLocation = JSON.parse(localStorage.getItem('previousPosition'));
        }

        localStorage.setItem('previousPosition', JSON.stringify([lat, lng]));

        // get old Location
        // update New Location



        // let marker = new google.maps.Marker({
        //     position: newPosition,
        //     map: mapRef.context.__SECRET_MAP_DO_NOT_USE_OR_YOU_WILL_BE_FIRED,
        //     icon: { url: icon },
        //     title: "title",
        //     rotation: bearing
        // });

        // setMapCarMarker(marker);
        if (previousLocation) {
            if (previousLocation.length > 0) {
                animate(previousLocation, newPosition, bearing, lat, lng)
            }
        }

    }
    function animate(previousLocation, newPosition, bearing, lat, lng) {
        // console.log(previousLocation)

        const speed = 70; // km/h
        // var car = "M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z";
        const symbol = {
            // path:MyCar,
            path: "M62.1,36.5c-0.9-1.2-3.6-1.5-3.6-1.5c0.1-3.5,0.5-6.9,0.7-8.2c1.9-7.3-1.7-11.8-1.7-11.8c-4.8-4.8-9.1-5-12.5-5   c-3.4,0-7.8,0.3-12.5,5c0,0-3.6,4.5-1.7,11.8c0.2,1.2,0.5,4.6,0.7,8.2c0,0-2.7,0.3-3.6,1.5c-0.9,1.2-0.9,1.9,0,1.9   c0.9,0,2.9-2.3,3.6-2.3V35c0,1,0.1,2,0.1,3c0,4.4,0,33.7,0,33.7s-0.3,6.1,5,7.8c1.2,0,4.6,0.4,8.4,0.5c3.8-0.1,7.3-0.5,8.4-0.5   c5.3-1.7,5-7.8,5-7.8s0-29.3,0-33.7c0-1,0-2,0.1-3v1.2c0.7,0,2.7,2.3,3.6,2.3C63,38.5,63,37.7,62.1,36.5z M34.7,66.5   c-0.3,3.3-2.3,4.1-2.3,4.1V37.4c0.8,1.2,2.3,6.8,2.3,6.8S34.9,63.2,34.7,66.5z M54.8,75.2c0,0-4.2,2.3-9.8,2.2   c-5.6,0.1-9.8-2.2-9.8-2.2v-2.8c4.9,2.2,9.8,2.2,9.8,2.2s4.9,0,9.8-2.2V75.2z M35.2,41.1l-1.7-10.2c0,0,4.5-3.2,11.5-3.2   s11.5,3.2,11.5,3.2l-1.7,10.2C51.4,39.2,38.6,39.2,35.2,41.1z M57.7,70.6c0,0-2.1-0.8-2.3-4.1c-0.3-3.3,0-22.4,0-22.4   s1.5-5.6,2.3-6.8V70.6z",
            // fillColor: '#FFFFFF',
            fillColor: '#3766DE',
            fillOpacity: 1,
            anchor: new google.maps.Point(45, 60),
            strokeWeight: 1,
            scale: .9,
            rotation: bearing
        }

        const delay = 100;

        if (marker != null) {

            marker.setMap(null);
        }

        marker = new google.maps.Marker({
            position: new google.maps.LatLng(previousLocation[0], previousLocation[1]),
            map: mapRef.context.__SECRET_MAP_DO_NOT_USE_OR_YOU_WILL_BE_FIRED,
            icon: symbol,

        });
        setMapCarMarker(marker);



        animateMarker(marker, [previousLocation, [lat, lng]], speed, previousLocation);


        function animateMarker(marker, coords, km_h, previousLocation) {
            let target = 0;
            var km_h = km_h || 50;

            function goToPoint() {
                const lastPosn = marker.getPosition();
                let lat = marker.getPosition().lat();
                let lng = marker.getPosition().lng();
                const step = (km_h * 1000 * delay) / 3600000; // in meters

                const start = new google.maps.LatLng(previousLocation[0], previousLocation[1]);
                const dest = new google.maps.LatLng(coords[target][0], coords[target][1]);

                const distance = google.maps.geometry.spherical.computeHeading(dest, marker.position); // in meters

                const numStep = distance / step;
                let i = 0;
                const deltaLat = (coords[target][0] - lat) / numStep;
                const deltaLng = (coords[target][1] - lng) / numStep;

                function moveMarker() {
                    lat += deltaLat;
                    lng += deltaLng;
                    i += step;

                    if (i < distance) {

                        const p = new google.maps.LatLng(lat, lng);
                        marker.setPosition(p);

                        var heading = google.maps.geometry.spherical.computeHeading(lastPosn, p);
                        symbol.rotation = heading;
                        marker.setIcon(symbol);

                        setTimeout(moveMarker, delay);
                    }
                    else {
                        marker.setPosition(dest);

                        var heading = google.maps.geometry.spherical.computeHeading(lastPosn, dest);
                        symbol.rotation = heading;
                        marker.setIcon(symbol);

                        target++;
                        if (target == coords.length) { target = coords.length; }

                        setTimeout(goToPoint, delay);
                    }
                }

                moveMarker();
            }
            goToPoint();
        }

    }


    function makeMarkerSource(newPosition, icon, bearing, lat, lng) {
        // console.log('********************************************************')
        // console.log(lat, lng);
        // console.log(JSON.parse(localStorage.getItem('previousPosition')));
        // console.log('********************************************************')
        // console.log(icon)
        //     var contentString = `<Box display="flex" justifyContent="flex-start" alignItems="center">           
        //     <Box>
        //         <div style={{ 'fontSize': '14px', 'overflow': 'hidden', 'padding': '15px 5px', 'margin': '0', }}>
        //            ${address}</div>
        //     </Box>
        // </Box>`;


        // var infowindow = new google.maps.InfoWindow({
        //     content: contentString
        // });
        // marker.addListener('click', function () {
        //     infowindow.open(mapRef.context.__SECRET_MAP_DO_NOT_USE_OR_YOU_WILL_BE_FIRED, marker);
        // });

        if (mapSourceMarker != null) mapSourceMarker.setMap(null);
        if (icon == IconCar) {
            let previousLocation;



            if (JSON.parse(localStorage.getItem('previousPosition'))) {

                previousLocation = JSON.parse(localStorage.getItem('previousPosition'));
            }

            localStorage.setItem('previousPosition', JSON.stringify([lat, lng]));

            if (previousLocation) {
                if (previousLocation.length > 0) {
                    animate(previousLocation, newPosition, bearing, lat, lng)
                }

            }
        }

        else {


            let marker = new google.maps.Marker({
                position: newPosition,
                map: mapRef.context.__SECRET_MAP_DO_NOT_USE_OR_YOU_WILL_BE_FIRED,
                icon: { url: icon },


            });

            if (marker != null) { setMapSourceMarker(marker); }
        }
    }

    function makeMarkerDestination(newPosition, icon, bearing, lat, lng) {
        // console.log(icon)
        //     var contentString = `<Box display="flex" justifyContent="flex-start" alignItems="center">           
        //     <Box>
        //         <div style={{ 'fontSize': '14px', 'overflow': 'hidden', 'padding': '15px 5px', 'margin': '0', }}>
        //            ${address}</div>
        //     </Box>
        // </Box>`;


        //     var infowindow = new google.maps.InfoWindow({
        //         content: contentString
        //     });

        if (mapDestinationMarker != null) mapDestinationMarker.setMap(null);
        if (mapSourceMarker != null) mapSourceMarker.setMap(null);
        if (icon == IconCar) {

            let previousLocation;
            if (JSON.parse(localStorage.getItem('previousPosition'))) {

                previousLocation = JSON.parse(localStorage.getItem('previousPosition'));
            }


            localStorage.setItem('previousPosition', JSON.stringify([lat, lng]));

            if (previousLocation) {
                if (previousLocation.length > 0) {
                    animate(previousLocation, newPosition, bearing, lat, lng)
                }
            }

        }
        else {


            let marker = new google.maps.Marker({
                position: newPosition,
                map: mapRef.context.__SECRET_MAP_DO_NOT_USE_OR_YOU_WILL_BE_FIRED,
                icon: { url: icon },


            });
            if (marker != null) { setMapDestinationMarker(marker); }
        }
    }


    useEffect(() => {

        if (Object.keys(pickUpSource).length && driverLocation && (!arrived)) {

            const directionsService = new google.maps.DirectionsService();

            directionsService.route({
                origin: { lat: pickUpSource.lat, lng: pickUpSource.lng },
                destination: { lat: driverLocation.socket.resource.latitude, lng: driverLocation.socket.resource.longitude },
                travelMode: google.maps.TravelMode.DRIVING,

            },
                (result, status) => {
                    if (status === google.maps.DirectionsStatus.OK) {

                        var leg = result.routes[0].legs[0];

                        if (mapSourceMarker != null) { mapSourceMarker.setMap(null) };
                        if (mapDestinationMarker != null) { mapDestinationMarker.setMap(null) };
                        let startPosition = new google.maps.LatLng(pickUpSource.lat, pickUpSource.lng);
                        let endPosition = new google.maps.LatLng(driverLocation.socket.resource.latitude, driverLocation.socket.resource.longitude);

                        makeMarkerSource(startPosition, DropIcon, driverLocation.socket.resource.bearing, driverLocation.socket.resource.latitude, driverLocation.socket.resource.longitude);
                        makeMarkerDestination(endPosition, IconCar, driverLocation.socket.resource.bearing, driverLocation.socket.resource.latitude, driverLocation.socket.resource.longitude);
                        setDir(result);

                    } else {
                        console.error(`error fetching directions ${result}`);
                    }
                }
            )


        }
        else {
            // console.log(pickUpSource,arrived ,startRide) ;
            if (driverLocation.socket !== undefined && (!startRide)) {

                if (mapCarMarker != null) mapCarMarker.setMap(null);
                if (mapDestinationMarker != null) mapDestinationMarker.setMap(null);
                if (mapSourceMarker != null) mapSourceMarker.setMap(null);
                setDir(null);

                let position = new google.maps.LatLng(driverLocation.socket.resource.latitude, driverLocation.socket.resource.longitude);

                makeMarkerCar(position, IconCar, driverLocation.socket.resource.bearing, driverLocation.socket.resource.latitude, driverLocation.socket.resource.longitude);
            }
        }

    }, [driverLocation, arrived]);

    useEffect(() => {

        if (arrived && driverLocation) {


            if (mapDestinationMarker != null) mapDestinationMarker.setMap(null);
            setDir(null);


            if (dropLocation.lat !== 0 && dropLocation.lng != 0 && (arrived) && startRide && Object.keys(driverLocation).length) {

                if (mapCarMarker != null) mapCarMarker.setMap(null);

                const directionsService = new google.maps.DirectionsService();

                directionsService.route({
                    origin: {
                        lat: driverLocation.socket.resource.latitude,
                        lng: driverLocation.socket.resource.longitude

                    },
                    destination: { lat: dropLocation.lat, lng: dropLocation.lng },
                    travelMode: google.maps.TravelMode.DRIVING,

                },
                    (result, status) => {
                        if (status === google.maps.DirectionsStatus.OK) {

                            var leg = result.routes[0].legs[0];


                            if (mapSourceMarker != null) { mapSourceMarker.setMap(null) };
                            if (mapDestinationMarker != null) { mapDestinationMarker.setMap(null) };

                            // let position = new google.maps.LatLng(driverLocation.socket.resource.latitude, driverLocation.socket.resource.longitude);

                            // makeMarkerSource(position, IconCar, driverLocation.socket.resource.bearing, driverLocation.socket.resource.latitude, driverLocation.socket.resource.longitude);


                            // // makeMarkerSource(leg.start_location, IconCar);
                            // makeMarkerDestination(leg.end_location, DropIcon);


                            let startPosition = new google.maps.LatLng(driverLocation.socket.resource.latitude, driverLocation.socket.resource.longitude);
                            let endPosition = new google.maps.LatLng(dropLocation.lat, dropLocation.lng);

                            makeMarkerSource(startPosition, IconCar, driverLocation.socket.resource.bearing, driverLocation.socket.resource.latitude, driverLocation.socket.resource.longitude);
                            makeMarkerDestination(endPosition, DropIcon, driverLocation.socket.resource.bearing, driverLocation.socket.resource.latitude, driverLocation.socket.resource.longitude);


                            setDir(result);

                        } else {
                            console.error(`error fetching directions ${result}`);
                        }
                    }
                )
            } else if (arrived && startRide) {


                if (driverLocation.socket !== undefined) {


                    if (mapCarMarker != null) mapCarMarker.setMap(null);

                    let position = new google.maps.LatLng(driverLocation.socket.resource.latitude, driverLocation.socket.resource.longitude);
                    makeMarkerCar(position, IconCar, driverLocation.socket.resource.bearing, driverLocation.socket.resource.latitude, driverLocation.socket.resource.longitude);
                }
            }

        }


    }, [driverLocation, startRide]);


    const connection = () => {

        // console.log('**************************************************');
        const socketConnection = socketIO('https://stagingapi.whipp.app');
        // const socketConnection = socketIO('http://13.52.206.171:3900');

        socketConnection.on('connect', () => {
            // console.log('**************************************************');
            // console.log('**************************************************');
            // console.log('************** Socket Connection *****************');
            // console.log('**************************************************');
            // console.log('**************************************************');

            setSocket(socketConnection);

        });
        // console.log('**************************************************');

    }


    const jobCompleted = () => {
        // console.log("Job completed");
        if (JSON.parse(localStorage.getItem('driverlocation'))) {
            const id = JSON.parse(localStorage.getItem('driverlocation'));

            socket.emit('stopRideTrackingEvent', { jobId: id.jobId });

            localStorage.removeItem('driverlocation');
            localStorage.removeItem('previousPosition');

        }
        if (mapRef) {
            if (mapRef.context.__SECRET_MAP_DO_NOT_USE_OR_YOU_WILL_BE_FIRED != null || mapRef.context.__SECRET_MAP_DO_NOT_USE_OR_YOU_WILL_BE_FIRED != undefined) {

                google.maps.event.trigger(mapRef.context.__SECRET_MAP_DO_NOT_USE_OR_YOU_WILL_BE_FIRED, 'resize');
            }
        }
        setText('Your Request has accepted');
        setlocation({});
        setDir(null);
        setpickUpSource({});
        setDropLocation({});
        setArrived(false);
        setStartRide(false);
        setDriverLocation({});
        if (mapCarMarker != null) { mapCarMarker.setMap(null) };
        if (mapSourceMarker != null) { mapSourceMarker.setMap(null) };
        if (mapDestinationMarker != null) { mapDestinationMarker.setMap(null) };
        setMapCarMarker(null);
        setMapSourceMarker(null);
        setMapDestinationMarker(null);
        if (marker != null) marker.setMap(null);


    }


    return (

        <Socket.Provider
            value={[socket, location, dir, pickUpSource, setpickUpSource, dropLocation, setDropLocation,
                forDirection, setArrived, setStartRide, jobCompleted, setDir, connection,
                rideAccepted, setRideAccepted, setMapRef, mapRef, startRide, text, setText]} > {props.children}
        </Socket.Provider>


    );
}

export default SocketContext;





