import React, { Fragment, useState, useRef, useEffect } from 'react';



import { connect } from "react-redux";

import 'react-toastify/dist/ReactToastify.css';
import ProtectedRoutes from './protected.routes';
import { BrowserRouter as Router, Switch, Route, Redirect, Link, useHistory, useLocation } from 'react-router-dom';

import AdminLayout from "layouts/Admin.js";
import AuthLayout from "layouts/Auth.js";
import GlobalStyle from './globalStyles';
import { ToastContainer, toast, Slide } from 'react-toastify';

function App(props) {

    useEffect(() => {
        // toast.success('Welcome Admin')

    }, [])



    console.log(props)


    const NotFoundPage = () => {
        return (
            <div>Not Found </div>
        )
    }

    return (
        <div>
            <ToastContainer transition={Slide} autoClose={1500} hideProgressBar={true} />
            <Switch>
                <ProtectedRoutes path="/admin" component={AdminLayout} {...props} />
                <ProtectedRoutes path="/auth" component={AuthLayout} {...props} />
                <Redirect from="/" to="/admin/index" />
                <Route path="*" component={NotFoundPage} />
            </Switch>
            <GlobalStyle />
        </div>
    );
}
const mapStateToProps = (state) => {
    return state;
}

export default connect(mapStateToProps)(App);
