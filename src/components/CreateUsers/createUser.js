/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { useState } from "react";

// reactstrap components
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    InputGroupAddon,
    InputGroupText,
    InputGroup,
    Row,
    Col
} from "reactstrap";
import { connect } from "react-redux";
import { subAdmin, profileImageUrl } from 'globalConstant';
import { GET, POST, PICTURE } from 'apiClient';
import { Route, Switch, Redirect, NavLink, Link } from "react-router-dom";
import { AppLoader } from '../../Context/appLoaderContext';
import { toast } from "react-toastify";

const CreateUser = (props) => {

    const [loaderOpen, loaderClose] = React.useContext(AppLoader);
    const [values, setValues] = useState({
        name: '',
        email: '',
        number: '',
        password: '',
        age: '',
        description: '',
        profileImage: '',
        institutionImage: ''

    })
    const [file, setFile] = useState('');
    const [instituteFile, setInstitutueFile] = useState('');

    const [permission, setPermission] = useState({
        images: 1,
        listing: 1,
        roles: 1

    })

    const handleChange = (e) => {
        e.preventDefault();

        setValues({
            ...values,
            [e.target.name]: e.target.value
        })


    }
    const permissions = (e) => {

        setPermission({
            ...permission,
            [e.target.name]: e.target.value
        })


    }
    const imageFile = (e) => {

        setFile(e.target.files[0]);
    }
    const instituteImageFile = (e) => {
        console.log(e.target.files)
        setInstitutueFile(e.target.files[0]);
    }
    const addFile = async () => {

        if (!file) return;

        const params = {
            image: file
        };
        const imgurl = await PICTURE(profileImageUrl, params).then(res => {

            if (res.data.success) {
                const { data: { data: { url } } } = res;
                // console.log(url);

                return url;
                // uploadImageUrl(url);
            }

        })
        return imgurl;
    }
    const instituteImage = async () => {

        if (!instituteFile) return;

        const params = {
            image: instituteFile
        };
        const imgurl = await PICTURE(profileImageUrl, params).then(res => {

            if (res.data.success) {
                const { data: { data: { url } } } = res;
                // console.log(url);

                return url;
                // uploadImageUrl(url);
            }

        })
        return imgurl;
    }

    const postData = (url, instUrl) => {



        let params = {
            name: values.name,
            email: values.email,
            password: values.password,
            age: values.age || '',
            description: values.description || '',
            phoneNumber: values.number,
            profileImage: url || ' ',
            institutionImage: instUrl || ' ',
            phoneNumberPrefix: "+92",
            rolesAndSecurity: permission.roles,
            imageUpload: permission.images,
            imagesListing: permission.listing,


        }




        POST(subAdmin, params).then(res => {

            console.log(res);
            if (res.data.success) {

                setValues({
                    ...values,
                    name: '',
                    email: '',
                    number: '',
                    password: '',
                    age: '',
                    description: '',
                    profileImage: '',
                    institutionImage: ''

                })
                setPermission({
                    ...permission,
                    images: 1,
                    listing: 1,
                    roles: 1

                })
                props.history.push('/admin/role');
                // props.dispatch({ type: 'login', payload: data.data });
                loaderClose();

            }
            else {
                console.log("error", res.data.message);
                toast.error(res.data.message)
                // console.log(socket);
                loaderClose();
            }


        });

    }

    const submit = async () => {

        if (values.name && values.email && values.password && values.number) {


            loaderOpen();

            const url = await addFile();
            const instUrl = await instituteImage();

            postData(url, instUrl);



        }

        else {
            toast.error("All fields are required")
        }



        // console.log(values)
        // console.log(permission)
        // loaderOpen();

        // let params = {
        //     name: values.name,
        //     email: values.email,
        //     password: values.password,
        //     phoneNumber: values.number,
        //     phoneNumberPrefix: "+92",
        //     rolesAndSecurity: permission.roles,
        //     imageUpload: permission.images,
        //     imagesListing: permission.listing
        // }



        // POST(subAdmin, params).then(res => {

        //     console.log(res);
        //     if (res.data.success) {

        //         setValues({
        //             ...values,
        //             name: '',
        //             email: '',
        //             number: '',
        //             password: '',
        //             profileImage: ''

        //         })
        //         setPermission({
        //             ...permission,
        //             images: 1,
        //             listing: 1,
        //             roles: 1

        //         })

        //         // props.dispatch({ type: 'login', payload: data.data });
        //         loaderClose();

        //     }
        //     else {
        //         console.log("error", res.message);

        //         // console.log(socket);
        //         loaderClose();
        //     }


        // });


    }


    return (
        <>
            <Link to="/admin/role" className="a-link p-3 btn-icon btn-2" color="primary" type="button">
                <span className="btn-inner--icon">
                    <i className="fa fa-angle-left" />
                </span>
                <span className="btn-inner--text">Back</span>
            </Link>
            <div className="d-flex justify-content-center align-items-center">
                <Card className="bg-secondary shadow border-0 mt-5 bg-white w-50">
                    <CardHeader className="bg-transparent pb-5">
                        <div className="text-muted text-center mt-2 mb-4">
                            <h1>Add New Role</h1>
                        </div>


                    </CardHeader>
                    <CardBody className="px-lg-5 py-lg-5">

                        <Row>

                            <Col >
                                <Form role="form">
                                    <FormGroup>
                                        <InputGroup className="input-group-alternative mb-3">
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText>
                                                    <i className="ni ni-hat-3" />
                                                </InputGroupText>
                                            </InputGroupAddon>
                                            <Input placeholder="Name" type="text" name="name" onChange={(e) => handleChange(e)} />
                                        </InputGroup>
                                    </FormGroup>
                                    <FormGroup>
                                        <InputGroup className="input-group-alternative mb-3">
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText>
                                                    <i className="ni ni-email-83" />
                                                </InputGroupText>
                                            </InputGroupAddon>
                                            <Input placeholder="Email" type="email" name="email" onChange={(e) => handleChange(e)} autoComplete="new-email" />
                                        </InputGroup>
                                    </FormGroup>
                                    <FormGroup>
                                        <InputGroup className="input-group-alternative">
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText>
                                                    <i className="ni ni-lock-circle-open" />
                                                </InputGroupText>
                                            </InputGroupAddon>
                                            <Input placeholder="Phone Number" name="number" type="phone" onChange={(e) => handleChange(e)} />
                                        </InputGroup>
                                    </FormGroup>
                                    <FormGroup>
                                        <InputGroup className="input-group-alternative">
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText>
                                                    <i className="ni ni-lock-circle-open" />
                                                </InputGroupText>
                                            </InputGroupAddon>
                                            <Input placeholder="Age" name="age" onChange={(e) => handleChange(e)} />
                                        </InputGroup>
                                    </FormGroup>
                                    <FormGroup>
                                        <InputGroup className="input-group-alternative">
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText>
                                                    <i className="ni ni-lock-circle-open" />
                                                </InputGroupText>
                                            </InputGroupAddon>
                                            <Input placeholder="Description" name="description" onChange={(e) => handleChange(e)} />
                                        </InputGroup>
                                    </FormGroup>
                                    <FormGroup>
                                        <InputGroup className="input-group-alternative">
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText>
                                                    <i className="ni ni-lock-circle-open" />
                                                </InputGroupText>
                                            </InputGroupAddon>
                                            <Input placeholder="Password" type="password" name="password" onChange={(e) => handleChange(e)} autoComplete="new-password" />
                                        </InputGroup>
                                    </FormGroup>

                                    <div className="d-flex align-items-center">

                                        <label htmlFor="img" className="profileImg">Profile image</label>
                                        <input type="file" id="img" name="img" accept="image/*" onChange={(e) => imageFile(e)} />
                                        <div className="ml-3">  {file.name}</div>
                                    </div>
                                    <div className="d-flex align-items-center">

                                        <label htmlFor="InstImg" className="profileImg">Institution image</label>
                                        <input type="file" id="InstImg" name="Instimg" accept="image/*" onChange={(e) => instituteImageFile(e)} />
                                        <div className="ml-3">  {instituteFile.name}</div>
                                    </div>

                                    <h1>Permissions</h1>

                                    <h5>Images Upload</h5>
                                    <div className=" d-flex">
                                        <div className="pr-3 custom-control custom-control-alternative custom-radio mb-3">
                                            <input
                                                className="custom-control-input"
                                                defaultChecked
                                                id="customRadio1"
                                                name="images"
                                                type="radio"
                                                value="1"
                                                onChange={(e) => permissions(e)}
                                            />
                                            <label className="custom-control-label" htmlFor="customRadio1">
                                                Don't Allow
                                         </label>
                                        </div>
                                        <div className="custom-control custom-control-alternative custom-radio mb-3">
                                            <input
                                                className="custom-control-input"
                                                value="2"
                                                id="customRadio2"
                                                name="images"
                                                type="radio"
                                                onChange={(e) => permissions(e)}
                                            />
                                            <label className="pr-3 custom-control-label" htmlFor="customRadio2">
                                                Allow
                                        </label>
                                        </div>
                                        {/* <div className="custom-control custom-control-alternative custom-radio mb-3">
                                            <input
                                                className="custom-control-input"
                                                value="3"
                                                id="customRadio3"
                                                name="images"
                                                type="radio"
                                                onChange={(e) => permissions(e)}
                                            />
                                            <label className="custom-control-label" htmlFor="customRadio3">
                                                Can edit
                                        </label>
                                        </div> */}
                                    </div>



                                    <h5>Images Listing</h5>
                                    <div className="d-flex">
                                        <div className="pr-3 custom-control custom-control-alternative custom-radio mb-3">
                                            <input
                                                className="custom-control-input"
                                                defaultChecked
                                                id="listing1"
                                                name="listing"
                                                type="radio"
                                                value="1"
                                                onChange={(e) => permissions(e)}
                                            />
                                            <label className="custom-control-label" htmlFor="listing1">
                                                Don't Show
                                         </label>
                                        </div>
                                        <div className="custom-control custom-control-alternative custom-radio mb-3">
                                            <input
                                                className="custom-control-input"
                                                value="2"
                                                id="listing2"
                                                name="listing"
                                                type="radio"
                                                onChange={(e) => permissions(e)}
                                            />
                                            <label className="pr-3 custom-control-label" htmlFor="listing2">
                                                Can View
                                        </label>
                                        </div>
                                        {/* <div className="custom-control custom-control-alternative custom-radio mb-3">
                                            <input
                                                className="custom-control-input"
                                                value="3"
                                                id="listing3"
                                                name="listing"
                                                type="radio"
                                                onChange={(e) => permissions(e)}
                                            />
                                            <label className="custom-control-label" htmlFor="listing3">
                                                Can edit
                                        </label>
                                        </div> */}
                                    </div>




                                    {/* 

                                    <h5>Roles</h5>
                                    <div className=" d-flex">
                                        <div className="pr-3 custom-control custom-control-alternative custom-radio mb-3">
                                            <input
                                                className="custom-control-input"
                                                defaultChecked
                                                id="customRadio4"
                                                name="roles"
                                                type="radio"
                                                value="1"
                                                onChange={(e) => permissions(e)}
                                            />
                                            <label className="custom-control-label" htmlFor="customRadio4">
                                                Don't Show
                                         </label>
                                        </div>
                                        <div className="custom-control custom-control-alternative custom-radio mb-3">
                                            <input
                                                className="custom-control-input"
                                                value="2"
                                                id="customRadio5"
                                                name="roles"
                                                type="radio"
                                                onChange={(e) => permissions(e)}
                                            />
                                            <label className="pr-3 custom-control-label" htmlFor="customRadio5">
                                                View
                                        </label>
                                        </div>
                                        <div className="custom-control custom-control-alternative custom-radio mb-3">
                                            <input
                                                className="custom-control-input"
                                                value="3"
                                                id="customRadio6"
                                                name="roles"
                                                type="radio"
                                                onChange={(e) => permissions(e)}
                                            />
                                            <label className="custom-control-label" htmlFor="customRadio6">
                                                Can edit
                                        </label>
                                        </div>
                                    </div>
 */}


                                    <div className="text-center">
                                        <Button block className="mt-4" color="primary" type="button"
                                            onClick={() => submit()}>
                                            Create role
                                         </Button>
                                    </div>




                                </Form>






                            </Col>






                        </Row>

                    </CardBody>
                </Card>
            </div >
        </>
    );

}
const mapStateToProps = (state) => {
    return state;
}

export default connect(mapStateToProps)(CreateUser);

