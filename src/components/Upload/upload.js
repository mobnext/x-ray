import React, { useEffect, useState } from 'react';
import './upload.css';
import { StyledDropZone } from 'react-drop-zone'
import 'react-drop-zone/dist/styles.css'
import { uploadImage, uploadUrl } from 'globalConstant';
import { GET, POST, PUT, PICTURE } from 'apiClient';
import { Route, Switch, Redirect, NavLink, Link } from "react-router-dom";
import { AppLoader } from '../../Context/appLoaderContext';
import { connect } from "react-redux";
import { toast } from 'react-toastify';


const Upload = () => {

    const [file, setFile] = useState([]);
    const [loaderOpen, loaderClose] = React.useContext(AppLoader);



    const addFile = (file) => {

        loaderOpen();
        setFile(file);
        const params = {
            image: file
        };
        PICTURE(uploadImage, params).then(res => {

            if (res.data.success) {
                const { data: { data: { url } } } = res;
                uploadImageUrl(url);
            }
            loaderClose();

        })

    }
    const uploadImageUrl = (url) => {

        let params = {
            url
        };

        POST(uploadUrl, params).then(res => {


            if (res.data.success) {

                toast.success(res.data.message);
                // props.dispatch({ type: 'login', payload: data.data });
                loaderClose();

            }
            else {
                console.log("error", res.message);

                // console.log(socket);
                loaderClose();
            }


        });

    }
    return (
        <div>
            <Link to="/admin/images" className="a-link p-3 btn-icon btn-2" color="primary" type="button">
                <span className="btn-inner--icon">
                    <i className="fa fa-angle-left" />
                </span>
                <span className="btn-inner--text">Back</span>
            </Link>

            <div className="wrapper">
                <div className="container"  >
                    <h1>
                        Upload an Image
                    </h1>
                    {/* <div className="upload-container" onDrop={(e) => addFile(e)} onDragLeave={(e) => handleFile(e)} onChange={() => uploadFile()}>
                        <div className="border-container">
                            <div className="icons fa-4x">
                                <i
                                    className="fas fa-file-image"
                                    data-fa-transform="shrink-1 down-5 left-6 rotate--45" />
                            
                            </div>
                            <input type="file" id="file-upload" onChange={() => uploadFile()} />
                            <p>
                                Drag and drop files here, or
                            <span id="file-browser">browse</span> your computer.
                            </p>
                        </div>
                    </div> */}
                    <div>
                        <StyledDropZone onDrop={(e) => addFile(e)} />
                        {file.name ?
                            <ul>

                                <li>
                                    <i className='fa fa-file' /> {file.name} [{file.type}]
              </li>


                            </ul> : null}
                    </div>
                </div>
            </div>

        </div>
    )
}
const mapStateToProps = (state) => {
    return state;
}

export default connect(mapStateToProps)(Upload);
