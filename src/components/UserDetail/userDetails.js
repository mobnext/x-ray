import React, { useState, useEffect } from "react";

// reactstrap components
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    InputGroupAddon,
    InputGroupText,
    InputGroup,
    Row,
    Col
} from "reactstrap";

import { subAdminUpdate, userDetail, manageAccount, profileImageUrl } from 'globalConstant';
import { GET, POST, PUT, PICTURE } from 'apiClient';
import { Route, Switch, Redirect, NavLink, Link } from "react-router-dom";
import { AppLoader } from '../../Context/appLoaderContext';
import { connect } from "react-redux";
import { toast } from 'react-toastify';

const UserDetail = (props) => {

    const [detail, setDetail] = useState({});
    const [loaderOpen, loaderClose] = React.useContext(AppLoader);

    const [file, setFile] = useState('');
    const [instituteFile, setInstitutueFile] = useState('');

    const [values, setValues] = useState({
        name: '',
        email: '',
        number: '',
        password: '',
        profileImage: '',
        institutionImage: ''

    });
    const [permission, setPermission] = useState({
        images: 1,
        listing: 1,
        roles: 1

    });
    useEffect(() => {
        getUserDetail();
    }, [])
    const getUserDetail = () => {

        loaderOpen();

        let params = {
            adminId: props.match.params.id,
        }
        POST(userDetail, params).then(res => {
            const { data } = res;

            console.log(data)
            if (data.success) {

                setDetail(data.data.subAdmin);

                setValues({
                    ...values,
                    name: data.data.subAdmin.name,
                    email: data.data.subAdmin.email,
                    number: data.data.subAdmin.phoneNumber,
                    password: '',
                    profileImage: data.data.subAdmin.profileImage,
                    institutionImage: data.data.subAdmin.institutionImage
                })
                setPermission({
                    ...permission,
                    images: data.data.subAdmin.imageUpload,
                    listing: data.data.subAdmin.imagesListing,
                    roles: data.data.subAdmin.rolesAndSecurity
                })
                loaderClose();

            }
            else {
                console.log("error", data.message);
                loaderClose();
                // console.log(socket);
            }

            //   setUser(true);

            // }).catch(err => {
            //   loaderClose();
            //   setUser(true);

        });
    }



    const handleChange = (e) => {
        e.preventDefault();
        setValues({
            ...values,
            [e.target.name]: e.target.value
        });

    }
    const permissions = (e) => {

        setPermission({
            ...permission,
            [e.target.id]: e.target.value
        });

    }
    const imageFile = (e) => {

        setFile(e.target.files[0]);
    }
    const instituteImageFile = (e) => {

        setInstitutueFile(e.target.files[0]);
    }
    const addFile = async () => {

        if (!file) return;

        const params = {
            image: file
        };
        const imgurl = await PICTURE(profileImageUrl, params).then(res => {

            if (res.data.success) {
                const { data: { data: { url } } } = res;
                // console.log(url);

                return url;
                // uploadImageUrl(url);
            }

        })
        return imgurl;
    }
    const instituteImage = async () => {

        if (!instituteFile) return;

        const params = {
            image: instituteFile
        };
        const imgurl = await PICTURE(profileImageUrl, params).then(res => {

            if (res.data.success) {
                const { data: { data: { url } } } = res;
                // console.log(url);

                return url;
                // uploadImageUrl(url);
            }

        })
        return imgurl;
    }

    const postData = (url, instUrl) => {


        let params = {
            adminId: props.match.params.id,
            name: values.name,
            email: values.email,
            password: values.password,
            phoneNumber: values.number,
            phoneNumberPrefix: "+92",
            profileImage: url || values.profileImage,
            institutionImage: instUrl || values.institutionImage,
            rolesAndSecurity: permission.roles,
            imageUpload: permission.images,
            imagesListing: permission.listing,
            status: 1
        }

        PUT(subAdminUpdate, params).then(res => {


            if (res.data.success) {

                setValues({
                    ...values,
                    name: '',
                    email: '',
                    number: '',
                    password: '',

                })
                setPermission({
                    ...permission,
                    images: 1,
                    listing: 1,
                    roles: 1

                })

                // props.dispatch({ type: 'login', payload: data.data });
                getUserDetail();
                toast.success('Update Successfully');
                props.history.push('/admin/role');
            }

            else if (res.data.response === 304) {
                loaderClose();
                toast.error(res.data.message);
                props.dispatch({ type: 'logout' });
                localStorage.removeItem('state');
            }
            else {
                console.log("error", res.data.message);
                toast.error(res.data.message);
                // console.log(socket);
                loaderClose();
            }


        });


    }
    const submit = async () => {

        if (values.name && values.email && values.number) {


            loaderOpen();

            const url = await addFile();
            const instUrl = await instituteImage();

            postData(url, instUrl);



        }

        else {
            toast.error("All fields are required")
        }





    }

    const accountSetting = (status) => {
        loaderOpen();

        let params = {
            adminId: props.match.params.id,
            status
        }
        PUT(manageAccount, params).then(res => {
            const { data } = res;
            console.log(res)
            if (data.success) {

                //   props.dispatch({ type: 'login', payload: data.data });

                toast.success(data.message);
                getUserDetail();
            }
            else {
                // console.log("error", data.message);
                toast.error(data.message)
                loaderClose();

                // console.log(socket);
            }

        })
    }
    return (
        <>
            <Link to="/admin/role" className="a-link p-3 btn-icon btn-2" color="primary" type="button">
                <span className="btn-inner--icon">
                    <i className="fa fa-angle-left" />
                </span>
                <span className="btn-inner--text">Back</span>
            </Link>

            <div className="d-flex justify-content-center align-items-center">
                <Card className="bg-secondary shadow border-0 mt-5 bg-white w-50">
                    <CardHeader className="bg-transparent pb-5">
                        <div className="d-flex justify-content-between text-muted text-center mt-2 mb-4">
                            <h1>User Details</h1>
                            {detail.isBlooked ? null :
                                <div>

                                    {detail.isApproved === false && detail.isRejected === true ?
                                        <Button onClick={() => accountSetting(1)}>Approved</Button>
                                        : null
                                    }

                                    {detail.isApproved === false && detail.isRejected === false ?
                                        <Button onClick={() => accountSetting(0)}>Reject</Button>
                                        : null
                                    }

                                    {detail.isApproved === true && detail.isRejected === false ?
                                        <Button onClick={() => accountSetting(2)}>Blocked</Button>
                                        : null
                                    }


                                </div>

                            }
                        </div>


                    </CardHeader>
                    <CardBody className="px-lg-5 py-lg-5">

                        <Row>

                            <Col >
                                <Form role="form">
                                    <FormGroup>
                                        <InputGroup className="input-group-alternative mb-3">
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText>
                                                    <i className="ni ni-hat-3" />
                                                </InputGroupText>
                                            </InputGroupAddon>
                                            <Input placeholder="Name" type="text" name="name" value={values.name} onChange={(e) => handleChange(e)} />
                                        </InputGroup>
                                    </FormGroup>
                                    <FormGroup>
                                        <InputGroup className="input-group-alternative mb-3">
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText>
                                                    <i className="ni ni-email-83" />
                                                </InputGroupText>
                                            </InputGroupAddon>
                                            <Input placeholder="Email" type="email" name="email" value={values.email} onChange={(e) => handleChange(e)} autoComplete="new-email" />
                                        </InputGroup>
                                    </FormGroup>
                                    <FormGroup>
                                        <InputGroup className="input-group-alternative">
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText>
                                                    <i className="ni ni-lock-circle-open" />
                                                </InputGroupText>
                                            </InputGroupAddon>
                                            <Input placeholder="Phone Number" name="number" type="phone" value={values.number} onChange={(e) => handleChange(e)} />
                                        </InputGroup>
                                    </FormGroup>
                                    <FormGroup>
                                        <InputGroup className="input-group-alternative">
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText>
                                                    <i className="ni ni-lock-circle-open" />
                                                </InputGroupText>
                                            </InputGroupAddon>
                                            <Input placeholder="Password" type="password" name="password" onChange={(e) => handleChange(e)} autoComplete="new-password" />
                                        </InputGroup>
                                    </FormGroup>


                                    <div className="d-flex align-items-center">

                                        <label htmlFor="img" className="profileImg">Profile image</label>
                                        <input type="file" id="img" name="img" accept="image/*" onChange={(e) => imageFile(e)} />
                                        <div className="ml-3">  {file.name}</div>
                                    </div>
                                    <div className="d-flex align-items-center">

                                        <label htmlFor="InstImg" className="profileImg">Institution image</label>
                                        <input type="file" id="InstImg" name="Instimg" accept="image/*" onChange={(e) => instituteImageFile(e)} />
                                        <div className="ml-3">  {instituteFile.name}</div>
                                    </div>

                                    {/* {detail.isBlooked ? null :
                                        <div>

                                            {detail.isApproved === false && detail.isRejected === true ?
                                                <Button onClick={() => accountSetting(1)}>Approved</Button>
                                                : null
                                            }

                                            {detail.isApproved === false && detail.isRejected === false ?
                                                <Button onClick={() => accountSetting(0)}>Reject</Button>
                                                : null
                                            }

                                            {detail.isApproved === true && detail.isRejected === false ?
                                                <Button onClick={() => accountSetting(2)}>Blocked</Button>
                                                : null
                                            }


                                        </div>

                                    } */}
                                    <h1>Permissions</h1>

                                    <h5>Images Upload</h5>
                                    <div className=" d-flex">
                                        <div className="pr-3 custom-control custom-control-alternative custom-radio mb-3">
                                            <input
                                                className="custom-control-input"
                                                // defaultChecked={}
                                                id="customRadio1"
                                                name="images"
                                                type="radio"
                                                value="1"
                                                onChange={(e) => setPermission({ ...permission, images: 1 })}
                                                checked={permission.images === 1}
                                            />
                                            <label className="custom-control-label" htmlFor="customRadio1">
                                                Don't Allow
                                         </label>
                                        </div>
                                        <div className="custom-control custom-control-alternative custom-radio mb-3">
                                            <input
                                                className="custom-control-input"
                                                value="2"
                                                id="customRadio2"
                                                name="images"
                                                type="radio"
                                                onChange={(e) => setPermission({ ...permission, images: 2 })}
                                                checked={permission.images === 2}
                                            />
                                            <label className="pr-3 custom-control-label" htmlFor="customRadio2">
                                                Allow
                                        </label>
                                        </div>
                                        {/* <div className="custom-control custom-control-alternative custom-radio mb-3">
                                            <input
                                                className="custom-control-input"
                                                value="3"
                                                id="customRadio3"
                                                name="images"
                                                type="radio"
                                                onChange={(e) => setPermission({ ...permission, images: 3 })}
                                                checked={permission.images === 3}
                                            />
                                            <label className="custom-control-label" htmlFor="customRadio3">
                                                Can edit
                                        </label>
                                        </div> */}
                                    </div>



                                    <h5>Images Listing</h5>
                                    <div className="d-flex">
                                        <div className="pr-3 custom-control custom-control-alternative custom-radio mb-3">
                                            <input
                                                className="custom-control-input"
                                                defaultChecked
                                                id="listing1"
                                                name="listing"
                                                type="radio"
                                                value="1"
                                                onChange={(e) => setPermission({ ...permission, listing: 1 })}
                                                checked={permission.listing === 1}
                                            />
                                            <label className="custom-control-label" htmlFor="listing1">
                                                Don't Show
                                         </label>
                                        </div>
                                        <div className="custom-control custom-control-alternative custom-radio mb-3">
                                            <input
                                                className="custom-control-input"
                                                value="2"
                                                id="listing2"
                                                name="listing"
                                                type="radio"
                                                onChange={(e) => setPermission({ ...permission, listing: 2 })}
                                                checked={permission.listing === 2}
                                            />
                                            <label className="pr-3 custom-control-label" htmlFor="listing2">
                                                View
                                        </label>
                                        </div>
                                        {/* <div className="custom-control custom-control-alternative custom-radio mb-3">
                                            <input
                                                className="custom-control-input"
                                                value="3"
                                                id="listing3"
                                                name="listing"
                                                type="radio"
                                                onChange={(e) => setPermission({ ...permission, listing: 3 })}
                                                checked={permission.listing === 3}
                                            />
                                            <label className="custom-control-label" htmlFor="listing3">
                                                Can edit
                                        </label>
                                        </div> */}
                                    </div>
                                    {/* 
                                    <h5>Roles</h5>
                                    <div className=" d-flex">
                                        <div className="pr-3 custom-control custom-control-alternative custom-radio mb-3">
                                            <input
                                                className="custom-control-input"
                                                defaultChecked
                                                id="customRadio4"
                                                name="roles"
                                                type="radio"
                                                value="1"
                                                onChange={(e) => setPermission({ ...permission, roles: 1 })}
                                                checked={permission.roles === 1}
                                            />
                                            <label className="custom-control-label" htmlFor="customRadio4">
                                                Don't Show
                                         </label>
                                        </div>
                                        <div className="custom-control custom-control-alternative custom-radio mb-3">
                                            <input
                                                className="custom-control-input"
                                                value="2"
                                                id="customRadio5"
                                                name="roles"
                                                type="radio"
                                                onChange={(e) => setPermission({ ...permission, roles: 2 })}
                                                checked={permission.roles === 2}
                                            />
                                            <label className="pr-3 custom-control-label" htmlFor="customRadio5">
                                                View
                                        </label>
                                        </div>
                                        <div className="custom-control custom-control-alternative custom-radio mb-3">
                                            <input
                                                className="custom-control-input"
                                                value="3"
                                                id="customRadio6"
                                                name="roles"
                                                type="radio"
                                                onChange={(e) => setPermission({ ...permission, roles: 3 })}
                                                checked={permission.roles === 3}
                                            />
                                            <label className="custom-control-label" htmlFor="customRadio6">
                                                Can edit
                                        </label>
                                        </div>
                                    </div>
 */}


                                    <div className="text-center">
                                        <Button block className="mt-4" color="primary" type="button"
                                            onClick={() => submit()}>
                                            Save
                                         </Button>
                                    </div>




                                </Form>






                            </Col>






                        </Row>

                    </CardBody>
                </Card>
            </div >
        </>
    );

}
// export default UserDetail;
const mapStateToProps = (state) => {
    return state;
}

export default connect(mapStateToProps)(UserDetail);