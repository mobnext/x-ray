import React, { useEffect, useState, memo } from "react";
// reactstrap components
import { Route, Switch, Redirect, NavLink, useHistory } from "react-router-dom";
import {
    Badge,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown,
    DropdownToggle,
    Media,
    Progress,
    Table, Card,
    UncontrolledTooltip,
    Container,
    Row,
    CardHeader, Button
} from "reactstrap";
import Detail from "components/Detail/detail";
import { getUserListing, deleteData } from 'globalConstant';
import { GET, POST } from 'apiClient';
import { AppLoader } from '../../Context/appLoaderContext';
import { toast } from 'react-toastify';
import { connect } from "react-redux";
import ReactPaginate from 'react-paginate';





const UserListing = memo((props) => {

    let history = useHistory();
    const [loaderOpen, loaderClose] = React.useContext(AppLoader);
    const addNew = () => {
        console.log(history)
        history.push(`/admin/role/new`)
    }
    const detail = (id) => {
        history.push(`/admin/role/${id}`)
    }

    const [usersData, setUserData] = useState([]);
    const [totalCounts, setTotalCounts] = useState(0);
    const [offset, setOffset] = useState(0);

    useEffect(() => {
        getUsers();


        return (() => {

        })

    }, [offset]);


    const getUsers = () => {
        loaderOpen();
        GET(`${getUserListing}/?limit=10&offset=${offset}`).then(res => {
            // const { data } = res;
            console.log(res)
            if (res.data.success) {

                //   props.dispatch({ type: 'login', payload: data.data });
                setUserData(res.data.data.subAdmin);
                setTotalCounts(res.data.data.subAdminCount);
                loaderClose();
            }
            else if (res.data.response === 304) {
                loaderClose();
                toast.error(res.data.message);
                props.dispatch({ type: 'logout' });
            }
            else {
                console.log("error", res.data.data.message);
                loaderClose();
                // console.log(socket);
            }

            //   setUser(true);

            // }).catch(err => {
            //   loaderClose();
            //   setUser(true);

        });
    }
    const handlePageClick = data => {
        let selected = data.selected;
        let offset = Math.ceil(selected * 10);
        console.log(offset)

        setOffset(offset);
        console.log(data)
    };

    const deleteItem = (val) => {
        console.log(val)

        // GET(`${deleteData}/?limit=10&offset=${offset}`).then(res => {
        //     // const { data } = res;
        //     console.log(res)
        //     if (res.data.success) {

        //         //   props.dispatch({ type: 'login', payload: data.data });
        //         setUserData(res.data.data.subAdmin);
        //         setTotalCounts(res.data.data.subAdminCount);
        //         loaderClose();
        //     }
        //     else if (res.data.response === 304) {
        //         loaderClose();
        //         toast.error(res.data.message);
        //         props.dispatch({ type: 'logout' });
        //     }
        //     else {
        //         console.log("error", res.data.data.message);
        //         loaderClose();
        //         // console.log(socket);
        //     }

        //     //   setUser(true);

        //     // }).catch(err => {
        //     //   loaderClose();
        //     //   setUser(true);

        // });
    }

    return (
        <>
            <Container className="mt-7" fluid>
                {/* Table */}
                <Row>
                    <div className="col">
                        <Card className="shadow">
                            <CardHeader className="border-0">
                                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <h3 className="mb-0">All Users</h3>
                                    {props.auth.currentUser.rolesAndSecurity !== 2 ?

                                        <Button
                                            className="mr-4"
                                            color="info"
                                            onClick={() => addNew()}
                                            size="sm"
                                        >
                                            Add New Role
                                    </Button>
                                        : null
                                    }



                                </div>
                            </CardHeader>
                        </Card>



                    </div>

                </Row>
            </Container>

            <Container fluid>
                <Card>
                    {usersData.length > 0 ?
                        <>
                            <Table className="align-items-center" responsive>
                                <thead className="thead-light">
                                    <tr>
                                        <th scope="col">Image</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Phone Number</th>
                                        {/* <th scope="col">Completion</th> */}
                                        <th scope="col" />
                                        {/* <th scope="col" /> */}
                                    </tr>
                                </thead>
                                <tbody>

                                    {usersData.map((val, index) => {

                                        return (


                                            <tr key={index}>
                                                <th scope="row">
                                                    <Media className="align-items-center">
                                                        <a
                                                            className="avatar rounded-circle mr-3"
                                                            href="#pablo"
                                                            onClick={e => e.preventDefault()}
                                                        >
                                                            <img
                                                                alt="..."
                                                                src={require("assets/img/theme/bootstrap.jpg")}
                                                            />
                                                        </a>
                                                        <Media>
                                                            <span className="mb-0 text-sm">

                                                            </span>
                                                        </Media>
                                                    </Media>
                                                </th>
                                                <td>{val.name}</td>


                                                <td>
                                                    <div className="d-flex align-items-center">
                                                        <span className="mr-2">{val.email}</span>

                                                    </div>
                                                </td>
                                                <td>

                                                    {val.isBlooked ?
                                                        <Badge color="" className="badge-dot mr-4">
                                                            <i className="bg-warning" />
                                                            {/* {val.status} */}
                                                            Blocked
                                                    </Badge> :

                                                        <>
                                                            {val.isApproved ?
                                                                <Badge color="" className="badge-dot mr-4">
                                                                    <i className="bg-success" />
                                                                    Approved
                                                    </Badge>
                                                                :

                                                                <Badge color="" className="badge-dot mr-4">
                                                                    <i className="bg-info" />
                                                                    {/* {val.status} */}
                                                                    Pending
                                                        </Badge>
                                                            }
                                                        </>


                                                    }
                                                </td>



                                                <td>
                                                    <div className="d-flex align-items-center">
                                                        <span className="mr-2">{val.phoneNumber}</span>

                                                    </div>
                                                </td>
                                                <td className="text-center">
                                                    {/* <UncontrolledDropdown>
                                        <DropdownToggle
                                            className="btn-icon-only text-light"
                                            href="#pablo"
                                            role="button"
                                            size="sm"
                                            color=""
                                            onClick={e => e.preventDefault()}
                                        >
                                            <i className="fas fa-ellipsis-v" />
                                        </DropdownToggle>
                                        <DropdownMenu className="dropdown-menu-arrow" right>
                                            <DropdownItem
                                                href="#pablo"
                                                onClick={e => e.preventDefault()}
                                            >
                                                Action
                    </DropdownItem>
                                            <DropdownItem
                                                href="#pablo"
                                                onClick={e => e.preventDefault()}
                                            >
                                                Another action
                    </DropdownItem>
                                            <DropdownItem
                                                href="#pablo"
                                                onClick={e => e.preventDefault()}
                                            >
                                                Something else here
                    </DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledDropdown>
                                   */}
                                                    {props.auth.currentUser.rolesAndSecurity !== 2 ?
                                                        <div onClick={() => detail(val._id)}>
                                                            <i className="fa fa-2x  fa-eye"></i>
                                                        </div>
                                                        : null}
                                                </td>
                                                {/* <td className="text-center">

                                                    {props.auth.currentUser.rolesAndSecurity !== 2 ?
                                                        <div onClick={() => deleteItem(val)}>
                                                            <i className="fa fa-2x  fa-trash"></i>
                                                        </div>
                                                        : null}
                                                </td> */}


                                            </tr>
                                        )
                                    })

                                    }


                                </tbody>
                            </Table>


                        </>


                        : null

                    }
                </Card>

                {totalCounts !== 0 ?
                    <div className="d-flex justify-content-center mt-3 ">


                        <ReactPaginate
                            previousLabel={<i className="fa fa-angle-left" />}
                            nextLabel={<i className="fa fa-angle-right" />}
                            breakLabel={'...'}
                            breakClassName={'break-me'}
                            pageCount={totalCounts / 10}
                            marginPagesDisplayed={2}
                            pageRangeDisplayed={5}
                            onPageChange={handlePageClick}
                            containerClassName={'pagination'}
                            subContainerClassName={'pages pagination'}
                            activeClassName={'pagination-active'}
                        />
                    </div>
                    : null}
            </Container>
        </>
    );

})
const mapStateToProps = (state) => {
    return state;
}

export default connect(mapStateToProps)(UserListing);