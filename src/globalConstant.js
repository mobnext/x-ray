
const signIn = 'admin/login';
const fetchProfile = 'admin/profile';
const getUserListing = 'sub-admin/admin/users';
const logoutUser = 'admin/logout';
const userDetail = 'sub-admin/admin/user';
const subAdmin = 'sub-admin/admin/new-user';
const subAdminUpdate = 'sub-admin/admin/update-user';
const uploadImage = 'upload/symptoms-image';
const uploadUrl = 'uploaded/symptoms-image-listing';
const imageListing = 'uploaded-data-listing';
const newUser = 'sub-admin/sign-up/sub-admin';
const manageAccount = 'sub-admin/accept-reject/toogle';
const profileImageUrl = 'upload/profile-image';
const deleteData = 'data-delete';
const dashboard = 'admin/dashboard';

export {
    signIn,
    fetchProfile,
    getUserListing,
    logoutUser,
    userDetail,
    subAdmin,
    subAdminUpdate,
    uploadImage,
    uploadUrl,
    imageListing,
    newUser,
    manageAccount,
    profileImageUrl,
    deleteData,
    dashboard


}