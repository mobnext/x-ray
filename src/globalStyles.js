import { createGlobalStyle } from 'styled-components';
// import fontUrls from './assetsfonts/UberMove-Bold.ttf';
// // import fontUrls1 from './assets/fonts/Amaranth-Bold.ttf';
// import fontUrls2 from './assets/fonts/UberMove-Medium.ttf';
// import fontUrls3 from './assets/fonts/UberMove-Regular.ttf';

const GlobalStyle = createGlobalStyle`
  html,
  body {
    // height: 100%;
    // width: 100%;     
    font-family: UberMove-Regular;
    background:#f7f7f7;
    
  }
  a {
    // color:yellow
  }
  .loader {
    height:100%;
    width:100%;
    background:black;
    opacity:0.8;
    position:fixed;
    z-index:1072;
    top:0;
    display:flex;
    justify-content:center;
    align-items:center
  }
 .navbar {
  padding-bottom: 0;
  padding-left: 5.5rem;
}
 
 .activeNavbar{
  border-bottom: 1px solid blue;
  padding: 1rem;
 }
 .nav-link {
  padding: 1rem;
 }
 .custom-control-label::after{
  border: 1px solid #8a8b92;
  border-radius: 50%;
 }
 .pagination-active {
  background:lightblue;
  padding:10px;
  color:white !important;
 }
 .pagination-active li a {
 
  color:white !important;
 }
//  .pagination-active:hover {
//    background:gray;
//    padding:10px
//  }
 .pagination li {
   padding:20px;
   border:1px solid gray;
   border-radius:50%;
   height:30px;
   width:30px;
   display:flex;
   justify-content:center;
   align-items:center;
   margin:5px
 }
 .profileImg {
  background: #5e72e4;
  padding: 10px;
  color: white;
  border-radius: 2px;
 }
 .a-link:hover {
  color: #233dd2 !important;
  text-decoration: none !important;
  background: #d6e5fa !important;
  margin: 2px;
  border-radius: 2px !important;
  transition:all .2s !important;
 }
  `;
export default GlobalStyle;