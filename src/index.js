/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import "assets/plugins/nucleo/css/nucleo.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "assets/scss/argon-dashboard-react.scss";
import { Provider } from 'react-redux'
import { createStore, combineReducers } from 'redux';
import rootReducer from './Reducers/rootReducer';
import authReducer from './Reducers/authReducer';

import { Router } from "react-router";
import { createBrowserHistory } from "history";

import App from './app';
import XRayLoader from './Context/appLoaderContext';
const history = createBrowserHistory();

const reducers = combineReducers({
  root: rootReducer,
  auth: authReducer
});

const loadState = () => {
  try {
    const serializedState = localStorage.getItem('state');

    if (serializedState === null) {
      return undefined;
    }

    return JSON.parse(serializedState);

  } catch (err) {
    return undefined;
  }
};

const saveState = (state) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem('state', serializedState);
  } catch (err) {

  }
};

const persistedState = loadState();

const store = createStore(reducers, persistedState)
store.subscribe(() => saveState(store.getState()));
ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <XRayLoader>
        <App />
      </XRayLoader>
    </Router>
  </Provider>,
  document.getElementById("root")
);

export default store;