/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { useEffect } from "react";
import { Route, Switch, Redirect, NavLink } from "react-router-dom";
// reactstrap components
import { Container } from "reactstrap";
// core components
import AdminNavbar from "components/Navbars/AdminNavbar.js";
import AdminFooter from "components/Footers/AdminFooter.js";
import Sidebar from "components/Sidebar/Sidebar.js";
import Detail from '../components/Detail/detail';
import Upload from '../components/Upload/upload';
import UserDetail from '../components/UserDetail/userDetails';
import CreateUser from '../components/CreateUsers/createUser';
import UserListing from '../components/UserListing/userListing';
import Tables from '../views/examples/Tables';
import Profile from '../views/examples/Profile';
import routes from "routes.js";
import Index from 'views/Index';
import { connect } from "react-redux";
import { toast } from 'react-toastify';
import { logoutUser } from 'globalConstant';
import { GET } from 'apiClient';






const Admin = (props) => {
  // console.log(props)
  const getRoutes = (routes) => {

    const NewRoutes = routes.slice(0, routes.length - 1);

    return NewRoutes.map((prop, key) => {
      if (prop.layout === "/admin") {
        return (
          <Route
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
            exact
          />
        );
      } else {
        return null;
      }
    });
  };
  const getBrandText = path => {
    for (let i = 0; i < routes.length; i++) {
      if (
        this.props.location.pathname.indexOf(
          routes[i].layout + routes[i].path
        ) !== -1
      ) {
        return routes[i].name;
      }
    }
    return "Brand";
  };

  useEffect(() => {
    toast.success('Welcome')

  }, [])

  const logout = () => {

    GET(logoutUser).then(res => {
      const { data } = res;
      if (data.success) {
        toast.success('Logout Successfully');
        props.dispatch({ type: 'logout' });
        localStorage.removeItem('state');
      }
      if (res.data.response === 304) {

        toast.error(res.data.message);
        props.dispatch({ type: 'logout' });
      }
      else {
        console.log("error", data.message);

        // console.log(socket);
      }

      //   setUser(true);

      // }).catch(err => {
      //   loaderClose();
      //   setUser(true);

    });



  }

  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light bg-white border border-bottom-light">
        <NavLink className="navbar-brand" to="/">X-rays</NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation">
          <span className="navbar-toggler-icon" />
        </button>
        <div
          className="collapse navbar-collapse"
          id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <NavLink to="/admin/index" className="nav-link" activeClassName="activeNavbar">
                Home <span className="sr-only">(current)</span>
              </NavLink>
            </li>

            {props.auth.currentUser.adminAccountType === 1 ? null :
              <>

                {(props.auth.currentUser.imagesListing !== 1 || props.auth.currentUser.imageUpload !== 1) ?
                  <li className="nav-item">
                    <NavLink to="/admin/images" className="nav-link" activeClassName="activeNavbar">Scanned Images</NavLink>

                  </li>
                  : null
                }
              </>
            }

            {props.auth.currentUser.adminAccountType === 1 ?

              <li className="nav-item">
                <NavLink to="/admin/role"

                  className="nav-link"
                  activeClassName="activeNavbar"
                  tabIndex={-1}
                  aria-disabled="true">Users</NavLink>
              </li>
              : null
            }



          </ul>
          <div className="form-inline my-2 my-lg-0">
            <ul className="navbar-nav mr-auto">
              {/* <li className="nav-item">
                <NavLink to="/admin/user-profile"
                  exact
                  className="nav-link "
                  activeClassName="activeNavbar"
                  tabIndex={-1}
                  aria-disabled="true">Profile</NavLink>
              </li> */}
              <li className="nav-item dropdown mr-8">
                <NavLink to="/admin/user-profile"
                  className="nav-link dropdown-toggle"
                  activeClassName="activeNavbar"
                  exact
                  id="navbarDropdown"
                  role="button"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false">
                  {props.auth.currentUser.name}
                </NavLink>
                <div
                  className="dropdown-menu"
                  aria-labelledby="navbarDropdown">

                  <NavLink to="/admin/user-profile"
                    exact
                    className="dropdown-item"
                  >Profile</NavLink>

                  <div className="dropdown-divider" />
                  <p className="dropdown-item" onClick={() => logout()}>
                    LogOut
                  </p>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <div className="main-content">


        <Switch>
          {props.auth.currentUser.adminAccountType === 2 ? <Route exact path="/admin/images/upload" component={Upload} /> : null}
          {props.auth.currentUser.adminAccountType === 2 ? <Route exact path="/admin/images/:id" component={Detail} /> : null}
          <Route exact path="/admin/role/new" component={CreateUser} />
          {props.auth.currentUser.adminAccountType === 1 ? <Route exact path="/admin/role/:id" component={UserDetail} /> : null}
          {props.auth.currentUser.adminAccountType === 2 ? <Route exact path="/admin/images" component={Tables} /> : null}
          {props.auth.currentUser.adminAccountType === 1 ? <Route exact path="/admin/role" component={UserListing} /> : null}
          <Route exact path="/admin/user-profile" component={Profile} />
          <Route exact path="/admin/index" component={Index} />
          <Redirect from="*" to="/admin/index" />
        </Switch>


      </div>
    </>
  );

}


const mapStateToProps = (state) => {
  return state;
}

export default connect(mapStateToProps)(Admin);
