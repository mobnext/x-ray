import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import AdminLayout from "../src/layouts/Admin";
import AuthLayout from "../src/layouts/Auth";

const ProtectedRoutes = ({ component: Component, ...rest }) => {

    return (
        <Route
            {...rest}
            render={props => {
                if (rest.auth.currentUser.isAuthenticated) {
                    return <AdminLayout {...props} />
                }
                else {
                    return <AuthLayout {...props} />
                }
            }}
        />
    );
}

const mapStateToProps = state => {
    return state.auth;
}
export default connect(mapStateToProps)(ProtectedRoutes);