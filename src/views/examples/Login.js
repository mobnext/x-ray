/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { useEffect, useState } from "react";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col
} from "reactstrap";
import { connect } from 'react-redux';
import { signIn } from 'globalConstant';
import { GET, POST } from 'apiClient';
import { Link } from "react-router-dom";
import { toast } from "react-toastify";


const Login = (props) => {
  // console.log(props);
  const [values, setValues] = useState({
    email: '',
    password: ''
  })


  const submit = () => {

    let params = {
      email: values.email,
      password: values.password
    }
    POST(signIn, params).then(res => {
      const { data } = res;
      // console.log(res)
      if (data.success) {

        props.dispatch({ type: 'login', payload: data.data });


      }
      else {
        // console.log("error", data.message);
        toast.error(data.message)

        // console.log(socket);
      }

      //   setUser(true);

      // }).catch(err => {
      //   loaderClose();
      //   setUser(true);

    });

  }

  return (
    <>
      <Col lg="8" md="8">
        <Card className="bg-secondary shadow border-0">
          <CardHeader className="bg-transparent pb-5">
            <div className="d-flex justify-content-center">
              <div>
                <img src={require('../../assets/img/brand/argon-react.png')} height="70px" />
              </div>
            </div>
            {/* <div className="text-muted text-center mt-2 mb-3">
              <small>Sign in with</small>
            </div>
            <div className="btn-wrapper text-center">
              <Button
                className="btn-neutral btn-icon"
                color="default"
                href="#pablo"
                onClick={e => e.preventDefault()}
              >
                <span className="btn-inner--icon">
                  <img
                    alt="..."
                    src={require("assets/img/icons/common/github.svg")}
                  />
                </span>
                <span className="btn-inner--text">Github</span>
              </Button>
              <Button
                className="btn-neutral btn-icon"
                color="default"
                href="#pablo"
                onClick={e => e.preventDefault()}
              >
                <span className="btn-inner--icon">
                  <img
                    alt="..."
                    src={require("assets/img/icons/common/google.svg")}
                  />
                </span>
                <span className="btn-inner--text">Google</span>
              </Button>
            </div> */}
          </CardHeader>

          <CardBody className="px-lg-5 py-lg-5">
            {/* <div className="text-center text-muted mb-4">
              <small>Or sign in with credentials</small>
            </div> */}
            <Form role="form">
              <FormGroup className="mb-3">
                <InputGroup className="input-group-alternative">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-email-83" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input placeholder="Email" type="email" autoComplete="new-email" onChange={(e) => setValues({ ...values, email: e.target.value })} />
                </InputGroup>
              </FormGroup>
              <FormGroup>
                <InputGroup className="input-group-alternative">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-lock-circle-open" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input placeholder="Password" type="password" autoComplete="new-password" onChange={(e) => setValues({ ...values, password: e.target.value })} />
                </InputGroup>
              </FormGroup>
              {/* <div className="custom-control custom-control-alternative custom-checkbox">
                <input
                  className="custom-control-input"
                  id=" customCheckLogin"
                  type="checkbox"
                />
                <label
                  className="custom-control-label"
                  htmlFor=" customCheckLogin"
                >
                  <span className="text-muted">Remember me</span>
                </label>
              </div> */}
              <div className="text-center">
                <Button className="my-4" color="primary" type="button" onClick={() => submit()}>
                  Sign in
                  </Button>
              </div>
            </Form>
          </CardBody>
        </Card>
        <Row className="mt-3">
          <Col xs="6">
            <small>

              <span className="text-dark font-weight-700"><Link to="/" className="text-dark text-underline">Forgot password</Link></span>
            </small>
          </Col>
          <Col className="text-right" xs="6">

            <small>

              <span className="text-dark font-weight-700"><Link to="/auth/register" className="text-dark text-underline">Create an account</Link></span>
            </small>
          </Col>
        </Row>
      </Col>
    </>
  );

}


const mapStateToProps = state => {
  return state.auth;
}
export default connect(mapStateToProps)(Login);