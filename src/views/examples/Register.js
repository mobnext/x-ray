/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { useEffect, useState } from "react";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col
} from "reactstrap";
import { Link } from "react-router-dom";
import { subAdmin, newUser } from 'globalConstant';
import { GET, POST } from 'apiClient';


const Register = (props) => {
  console.log(props)


  const [values, setValues] = useState({
    name: '',
    email: '',
    password: '',
    fileName: '',
    age: '',
    description: ''
  });
  const imageFile = (e) => {
    console.log(e.target.files)
    setValues({
      ...values,
      fileName: e.target.files[0].name
    })
  }
  const submit = () => {
    console.log(values)

    // loaderOpen();
    let params = {
      name: values.name,
      email: values.email,
      password: values.password,
      phoneNumber: values.number,
      phoneNumberPrefix: "+92",
      age: values.age,
      description: values.description
      // rolesAndSecurity: permission.roles,
      // imageUpload: permission.images,
      // imagesListing: permission.listing
    }
    POST(newUser, params).then(res => {

      // console.log(res);
      if (res.data.success) {

        setValues({
          ...values,
          name: '',
          email: '',
          password: '',
          fileName: '',
          age: '',
          description: ''

        })

        props.history.push('/auth/login');
        // props.dispatch({ type: 'login', payload: data.data });
        // loaderClose();

      }
      else {
        console.log("error", res.message);

        // console.log(socket);
        // loaderClose();
      }


    });


  }


  return (
    <>
      <Col lg="8" md="8">
        <Card className="bg-secondary shadow border-0">
          <CardHeader className="bg-transparent pb-5">
            <div className="d-flex justify-content-center">
              <div>
                <img src={require('../../assets/img/brand/argon-react.png')} height="70px" />
              </div>
            </div>
            {/* <div className="text-muted text-center mt-2 mb-4">
                <small>Sign up with</small>
              </div>
              <div className="text-center">
                <Button
                  className="btn-neutral btn-icon mr-4"
                  color="default"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <span className="btn-inner--icon">
                    <img
                      alt="..."
                      src={require("assets/img/icons/common/github.svg")}
                    />
                  </span>
                  <span className="btn-inner--text">Github</span>
                </Button>
                <Button
                  className="btn-neutral btn-icon"
                  color="default"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <span className="btn-inner--icon">
                    <img
                      alt="..."
                      src={require("assets/img/icons/common/google.svg")}
                    />
                  </span>
                  <span className="btn-inner--text">Google</span>
                </Button>
              </div> */}
          </CardHeader>
          <CardBody className="px-lg-5 py-lg-5">
            <div className="text-center text-muted mb-4">
              {/* <small>Or sign up with credentials</small> */}
            </div>
            <Form role="form">
              <FormGroup>
                <InputGroup className="input-group-alternative mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-hat-3" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input placeholder="Name" type="text" onChange={(e) => setValues({ ...values, name: e.target.value })} />
                </InputGroup>
              </FormGroup>
              <FormGroup>
                <InputGroup className="input-group-alternative mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-email-83" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input placeholder="Email" type="email" autoComplete="new-email" onChange={(e) => setValues({ ...values, email: e.target.value })} />
                </InputGroup>
              </FormGroup>
              <FormGroup>
                <InputGroup className="input-group-alternative mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-email-83" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input placeholder="Phone Number" type="phone" autoComplete="new-email" onChange={(e) => setValues({ ...values, number: e.target.value })} />
                </InputGroup>
              </FormGroup>
              <FormGroup>
                <InputGroup className="input-group-alternative">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-lock-circle-open" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input placeholder="Password" type="password" autoComplete="new-password" onChange={(e) => setValues({ ...values, password: e.target.value })} />
                </InputGroup>
              </FormGroup>
              <FormGroup>
                <InputGroup className="input-group-alternative">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-lock-circle-open" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input placeholder="age" autoComplete="new-password" onChange={(e) => setValues({ ...values, age: e.target.value })} />
                </InputGroup>
              </FormGroup>
              <FormGroup>
                <InputGroup className="input-group-alternative">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-lock-circle-open" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input placeholder="description" autoComplete="new-password" onChange={(e) => setValues({ ...values, description: e.target.value })} />
                </InputGroup>
              </FormGroup>

              <div className="d-flex align-items-center">

                <label for="img" className="profileImg">Select image</label>
                <input type="file" id="img" name="img" accept="image/*" onChange={(e) => imageFile(e)} />
                <div className="ml-3">  {values.fileName}</div>
              </div>
              <div className="text-center">
                <Button className="mt-4" color="primary" type="button" onClick={submit}>
                  Create account
                  </Button>
              </div>

              <div className="text-dark mt-3">
                <small>

                  <span className="text-dark font-weight-700">Already have an account?<Link to="/auth/login" className="text-dark text-underline">Login here</Link></span>
                </small>
              </div>
            </Form>
          </CardBody>
        </Card>
      </Col>
    </>
  );

}

export default Register;
