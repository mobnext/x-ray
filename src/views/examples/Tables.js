/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { useState, useEffect } from "react";
import { Route, Switch, Redirect, NavLink, useHistory } from "react-router-dom";

// reactstrap components
import {
  Badge,
  Card,
  CardHeader, CardImg,
  CardFooter,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Media,
  Pagination,
  PaginationItem,
  PaginationLink,
  Progress,
  Table, Button,
  Container,
  Row,
  CardBody,
  CardTitle,
  Col,
  CardText
} from "reactstrap";
// core components
import { imageListing, deleteData } from 'globalConstant';
import { GET, POST, PUT } from 'apiClient';

import { AppLoader } from '../../Context/appLoaderContext';
import { connect } from "react-redux";
import { toast } from 'react-toastify';
import ReactPaginate from 'react-paginate';
import * as moment from 'moment';

const Tables = (props) => {

  const [loaderOpen, loaderClose] = React.useContext(AppLoader);
  const [listing, setListing] = useState([]);
  const [detail, setDetail] = useState(false);
  const [detailData, setDetailData] = useState({});
  const [totalCounts, setTotalCounts] = useState(0);
  const [offset, setOffset] = useState(0);



  useEffect(() => {
    getImagesListing();
  }, [offset]);

  const getImagesListing = () => {

    loaderOpen();

    GET(`${imageListing}/?limit=10&&offset=${offset}`).then(res => {

      if (res.data.success) {
        const { data: { data: { fetchDataListing } } } = res;
        setListing(fetchDataListing);
        setTotalCounts(res.data.data.totalData)
        loaderClose();

      }
      else {
        console.log("error", res.data.message);
        loaderClose();
        // console.log(socket);
      }

      //   setUser(true);

      // }).catch(err => {
      //   loaderClose();
      //   setUser(true);

    });
  }
  let history = useHistory();
  const details = (data) => {
    console.log(history);
    setDetail(true);
    setDetailData(data);
    // history.push(`/admin/images/${id}`);
  }
  const upload = () => {

    history.push('/admin/images/upload');
  }
  const handlePageClick = data => {
    let selected = data.selected;
    let offset = Math.ceil(selected * 10);
    console.log(offset)

    setOffset(offset);
    console.log(data)
  };
  const itemDelete = (val) => {
    console.log(val);
    GET(`${deleteData}/${val._id}`).then(res => {
      // const { data } = res;
      console.log(res)
      if (res.data.success) {
        getImagesListing();
        //   props.dispatch({ type: 'login', payload: data.data });
        toast.success(res.data.message);


      }
      else if (res.data.response === 304) {

        toast.error(res.data.message);

      }
      else {
        console.log("error", res.data.data.message);

        // console.log(socket);
      }

      //   setUser(true);

      // }).catch(err => {
      //   loaderClose();
      //   setUser(true);

    });
  }



  return (
    <>
      {/* <Header /> */}
      {/* Page content */}


      {detail ? <a onClick={() => setDetail(false)} className="a-link p-3 btn-icon btn-2" color="primary" type="button">
        <span className="btn-inner--icon">
          <i className="fa fa-angle-left" />
        </span>
        <span className="btn-inner--text">Back</span>
      </a>
        : null}


      {/* <Link to="/admin/role" className="p-3 btn-icon btn-2" color="primary" type="button">
        <span className="btn-inner--icon">
          <i className="fa fa-angle-left" />
        </span>
        <span className="btn-inner--text">Back</span>
      </Link> */}





      {detail ?

        <Container className="mt-4" fluid>
          <Row>
            <Col md="4" xl="3" >
              <Card>
                <CardBody>
                  <CardImg
                    alt="..."
                    src={detailData.symptomImg || require("assets/img/brand/image2.jpg")}
                    top
                    style={{
                      objectFit: 'cover',
                      // height: '50vh'
                    }}
                  />
                </CardBody>
              </Card>
              {/* <img src={detailData.symptomImg} /> */}
            </Col>
            <Col md="8" xl="9" >
              <Card>
                <CardBody>
                  <h1>Details</h1>

                  <div>
                    <h3>
                      probability
                      </h3>
                    <h5>{detailData.probability}</h5>
                  </div>
                  <div>
                    <h3>
                      Symptoms
                      </h3>
                    <h5>{detailData.symptom || 'No symptoms Found'}</h5>
                  </div>

                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>

        :
        <>
          <Container className="mt-4" fluid>

            <Row>
              <div className="col">
                <Card className="shadow">
                  <CardHeader className="border-0">
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                      <h3 className="mb-0">Images Listing</h3>

                      {props.auth.currentUser.imageUpload !== 1 ?
                        <Button
                          className="mr-4"
                          color="info"
                          onClick={() => upload()}
                          size="sm"
                        >
                          Upload Image
                    </Button>
                        : null
                      }
                    </div>
                  </CardHeader>
                </Card>



              </div>

            </Row>
          </Container>

          {/* 
          {props.auth.currentUser.imagesListing !== 1 ?
            <Container className="mt-5 mb-2" fluid>
              <Row >

                {listing.map((val, index) => {
                  return (

                    <div className="mb-3 col-lg-4 col-md-6 col-sm-6 col-xl-3" key={index}>
                      <Card style={{ cursor: 'pointer' }} className="card h-100" onClick={() => details(val)}>
                        <CardImg
                          alt="..."
                          src={val.symptomImg || require("assets/img/brand/image2.jpg")}
                          top
                          style={{
                            objectFit: 'cover',
                            height: '30vh'
                          }}
                        />
                        <CardBody>
                          <CardTitle>Card title</CardTitle>
                          <CardText>
                            Some quick example text to build on the card title and make up
                            the bulk of the card's content.
                  </CardText>

                        </CardBody>
                      </Card>
                    </div>

                  )


                })
                }

              </Row>

            </Container>
            : null} */}











          <Container fluid>
            <Card>
              {listing.length > 0 ?
                <>
                  <Table className="align-items-center" responsive>
                    <thead className="thead-light">
                      <tr>
                        <th scope="col">Symptom Image</th>
                        <th scope="col">Institution Image</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Symptom</th>
                        <th scope="col">Probability</th>
                        <th scope="col">Time</th>
                        {/* <th scope="col">Completion</th> */}
                        <th scope="col" />
                        <th scope="col" />
                      </tr>
                    </thead>
                    <tbody>

                      {listing.map((val, index) => {

                        return (


                          <tr key={index}>
                            <th scope="row">
                              <Media className="align-items-center">
                                <a
                                  className="avatar rounded-circle mr-3"
                                  href="#pablo"
                                  onClick={e => e.preventDefault()}
                                >
                                  <img
                                    alt="..."
                                    style={{ objectFit: 'cover', height: '100%' }}
                                    src={val.symptomImg || require("assets/img/theme/bootstrap.jpg")}
                                  />
                                </a>
                                <Media>
                                  <span className="mb-0 text-sm">

                                  </span>
                                </Media>
                              </Media>
                            </th>
                            <th scope="row">
                              <Media className="align-items-center">
                                <a
                                  className="avatar rounded-circle mr-3"
                                  href="#pablo"
                                  onClick={e => e.preventDefault()}
                                >
                                  <img
                                    alt="..."
                                    style={{ objectFit: 'cover', height: '100%' }}
                                    src={val.institutionImage || require("assets/img/theme/bootstrap.jpg")}
                                  />
                                </a>
                                <Media>
                                  <span className="mb-0 text-sm">

                                  </span>
                                </Media>
                              </Media>
                            </th>



                            <td>{val.name}</td>


                            <td>
                              <div className="d-flex align-items-center">
                                <span className="mr-2">{val.email}</span>

                              </div>
                            </td>
                            <td>

                              {val.symptom}
                            </td>



                            <td>
                              <div className="d-flex align-items-center">
                                <span className="mr-2">{val.probability}</span>

                              </div>
                            </td>

                            <td>
                              {/* {moment(val.creationTimeStamp)} */}
                              {`${moment(val.creationTimeStamp * 1000).format('MMM DD, hh:mm a')}`}
                            </td>
                            <td className="text-center">

                              {props.auth.currentUser.rolesAndSecurity !== 2 ?
                                <div onClick={() => details(val)}>
                                  <i className="fa fa-2x  fa-eye"></i>
                                </div>
                                : null}
                            </td>
                            <td className="text-center">

                              {props.auth.currentUser.rolesAndSecurity !== 2 ?
                                <div onClick={() => itemDelete(val)}>
                                  <i className="fa fa-2x  fa-trash"></i>
                                </div>
                                : null}
                            </td>

                          </tr>
                        )
                      })

                      }


                    </tbody>
                  </Table>


                </>


                : <div>No records Found</div>

              }
            </Card>
            {totalCounts !== 0 && listing.length > 0 ?
              <div className="d-flex justify-content-center mt-3 ">
                <ReactPaginate
                  previousLabel={<i className="fa fa-angle-left" />}
                  nextLabel={<i className="fa fa-angle-right" />}
                  breakLabel={'...'}
                  breakClassName={'break-me'}
                  pageCount={totalCounts / 10}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={5}
                  onPageChange={handlePageClick}
                  containerClassName={'pagination'}
                  subContainerClassName={'pages pagination'}
                  activeClassName={'pagination-active'}
                />
              </div>
              : null}

          </Container>
        </>
      }


    </>
  );

}

const mapStateToProps = (state) => {
  return state;
}

export default connect(mapStateToProps)(Tables);

